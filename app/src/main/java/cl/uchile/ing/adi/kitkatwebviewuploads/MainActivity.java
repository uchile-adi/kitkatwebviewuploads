package cl.uchile.ing.adi.kitkatwebviewuploads;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;


public class MainActivity extends ActionBarActivity {
    private static Set<String> buggyVersions = new HashSet<String>(Arrays.asList("4.4.0","4.4.1","4.4.2","4.4.3"));
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buggyButton = (Button) this.findViewById(R.id.button_buggy);
        Button fixButton = (Button) this.findViewById(R.id.button_fix);

        buggyButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), BuggyWebviewActivity.class);
                startActivity(i);
            }
        });

        fixButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), KitKatWebviewActivity.class);
                startActivity(i);
            }
        });

        TextView textViewVersion = (TextView) this.findViewById(R.id.textViewVersion);
        TextView textViewMsg = (TextView) this.findViewById(R.id.textViewMsg);
        textViewVersion.setText(String.format(Locale.getDefault(), getString(R.string.main_version), Build.VERSION.RELEASE, Build.VERSION.SDK_INT));
        textViewMsg.setText(buggyVersions.contains(Build.VERSION.RELEASE) ? R.string.main_msg_positive : R.string.main_msg_negative);
    }
}
