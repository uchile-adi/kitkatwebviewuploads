package cl.uchile.ing.adi.kitkatwebviewuploads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.widget.Toast;

import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * This class extends your WebViewClient and implements the behavior
 * needed to fix the KitKat WebView bug
 */
public class KitKatWebViewClient extends MyWebViewClient {
    private static final String MIME_TYPE = "text/html";
    private static final String CHARSET = "ISO-8859-1";//If your server uses UTF-8 you should update this
    private static final String TEXT_HTML_CHARSET = String.format(Locale.getDefault(), "%s; charset=%s", MIME_TYPE, CHARSET );

    private Map<String, Uri> files = new HashMap<String, Uri>();
    private Activity activity;

    public KitKatWebViewClient(Activity activity){
        this.activity = activity;
    }

    public void onPageFinished(WebView view, String url){
        super.onPageFinished(view,url);
        String chooseFileStr = activity.getString(R.string.webview_file_chooser);
        //We use prototype to simplify DOM manipulation, you are free to use other libraries
        //We traverse all current input type=file,
        //for each of them we convert the <input type="file"> into <input type="button">
        //Then we assign a JavaScriptCore method to be called when clicked.
        //We also put a hidden tag to mark inside the form to inform our WebClient this view needs to be intercepted.
        view.loadUrl("javascript:$$('input[type=\"file\"]').each(function(f){var f1 = f; f.type='button'; f.value='"+chooseFileStr+"'; f.on('click', function() {KitKatUploads.upload(f1.getAttribute('id'), f1.getAttribute('name'));}); f = f.up('form'); f.insert('<input type=\"hidden\" name=\"shouldInterceptRequest\" value=\"1\">' ); f.setAttribute('method','get'); });");
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        if( ! url.contains("shouldInterceptRequest=1") ) return null;

        MultipartBuilder multipart = new MultipartBuilder().type(MultipartBuilder.FORM);

        Uri uri = Uri.parse(url);
        for( String key: uri.getQueryParameterNames() ) {
            if( ! key.equals("shouldInterceptRequest") ) multipart.addFormDataPart( key, uri.getQueryParameter(key) );
        }
        for( String name: this.files.keySet() ) {
            String filename = null;
            try {
                filename = getRealPathFromURI(activity, this.files.get(name));
                InputStream is = activity.getContentResolver().openInputStream(this.files.get(name));
                multipart.addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"" + name + "\"; filename=\"" + filename + "\""),
                        RequestBody.create(null, IOUtils.toByteArray(is))
                );
            } catch (IOException e) {
                e.printStackTrace();
                (new Handler(activity.getMainLooper())).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity.getApplicationContext(), R.string.webview_problem_loading_file, Toast.LENGTH_LONG).show();
                    }
                });
                String res = String.format(Locale.getDefault(), "<html><head></head><body><h1>Error</h1><p>We could not open file: %s</p><p>Please try another file</p></body></html>", filename);
                InputStream stream = new ByteArrayInputStream(res.getBytes());
                return new WebResourceResponse(MIME_TYPE, CHARSET, stream);
            }
        }
        Request request = new Request.Builder()
                .url(url.substring(0, url.indexOf("?")))
                .post( multipart.build() )
                .build();

        OkHttpClient client = new OkHttpClient();
        try {
            Response response = client.newCall(request).execute();
            if( response.isSuccessful() ) {
                //We try to get the mimeType and encoding from the server
                String[] ct = response.header("Content-Type", TEXT_HTML_CHARSET).split("; charset=");
                InputStream is = response.body().byteStream();
                if(ct.length==2) return new WebResourceResponse(ct[0], ct[1], is );
                else return new WebResourceResponse(MIME_TYPE, CHARSET, is );//Fallback to our default
            }
        } catch ( Exception e ) {
        }

        return null;
    }


    public void putFile(String name, Uri uri) {
        this.files.put(name, uri);
    }

    /**
     * Utility method to get the real filename from an Android Uri
     * This method is static and can be moved to other place, we keep it here to avoid creating
     * more classes
     *
     * @param context any context
     * @param uri The Android Uri to resolve
     * @return the real name of the file
     */
    public static String getRealPathFromURI(Context context, Uri uri) {
        String name = "";
        try {
            Cursor cursor = context.getContentResolver().query( uri, null, null, null, null );
            cursor.moveToFirst();
            name = cursor.getString( cursor.getColumnIndex( OpenableColumns.DISPLAY_NAME ));
            cursor.close();
        } catch( Exception e ) {
            name = ( new File( uri.toString() ) ).getName();
        }
        return name;
    }
}
