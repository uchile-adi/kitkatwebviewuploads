package cl.uchile.ing.adi.kitkatwebviewuploads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * This is the way we usually implement the actions related to <input type="file> elements
 * within our webviews. Unfortunately, this solution does not work on Android KitKat.
 */
public class BuggyWebviewActivity extends ActionBarActivity {
    private ValueCallback<Uri> inputFile;
    private ValueCallback<Uri[]> inputFile5;
    private WebView webView;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        webView = (WebView) this.findViewById(R.id.webview);
        webView.setWebViewClient(new MyWebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            //The undocumented magic method override Eclipse will swear at you if you try to put @Override here
            // For Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                fileChooser(uploadMsg);
            }

            // For Android 3.0+
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                fileChooser(uploadMsg);
            }

            //For Android 4.1
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture){
                fileChooser(uploadMsg);
            }

            //For Android 5.0+
            @SuppressLint("NewApi")
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> uploadMsg, WebChromeClient.FileChooserParams params) {
                inputFile5 = uploadMsg;
                fileChooser(null);
                return true;
            }
        } );
        webView.loadUrl("http://users.dcc.uchile.cl/~acadiz/files.php");
    }

    /**
     * Generic FileChooser opener.
     * @param c a ValueCallback<Uri> received from one of the callbacks defined on WebChromeClient
     */
    public void fileChooser(ValueCallback<Uri> c) {
        inputFile = c;
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("*/*");
        startActivityForResult(Intent.createChooser(i, this.getString(R.string.webview_file_chooser)), 1);
    }

    /**
     * Here we'll receive the response from the system's FileChooser. If you are receiving
     * repsonses from other external Activities, you'll need to check the requestCode
     *
     * @param requestCode the same given at startActivityForResult, we are using the value 1 for this example
     * @param resultCode tells whether the operation was successful.
     * @param intent the response Intent
     */
    @Override public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK && intent != null && intent.getData() != null) {
            Uri result = intent.getData();
            Uri[] result5 = new Uri[]{ result };
            if(inputFile != null) inputFile.onReceiveValue(result);
            if(inputFile5 != null) inputFile5.onReceiveValue(result5);
        }
        else if(resultCode == Activity.RESULT_CANCELED){
            if(inputFile != null) inputFile.onReceiveValue(null);
        }
        inputFile = null;
        inputFile5 = null;
    }
}

