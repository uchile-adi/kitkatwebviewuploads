package cl.uchile.ing.adi.kitkatwebviewuploads;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;


public class KitKatWebviewActivity extends ActionBarActivity {
    private WebView webView;
    private KitKatWebViewClient kitKatWebViewClient;
    private ValueCallback<Uri> inputFile;

    @SuppressLint({ "SetJavaScriptEnabled" })
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        webView = (WebView) this.findViewById(R.id.webview);
        kitKatWebViewClient = new KitKatWebViewClient(this);
        webView.setWebViewClient(kitKatWebViewClient); //We use out new WebViewClient which includes the KitKat workaround
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "KitKatUploads");// We need to attach our Javascript interface to catch the upload() method calls

        webView.loadUrl("http://users.dcc.uchile.cl/~acadiz/files.php");
    }

    /**
     * Generic FileChooser opener.
     * @param c a ValueCallback<Uri> received from one of the callbacks defined on WebChromeClient
     */
    public void fileChooser(ValueCallback<Uri> c) {
        inputFile = c;
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("*/*");
        startActivityForResult(Intent.createChooser(i, this.getString(R.string.webview_file_chooser)), 1);
    }

    /**
     * Here we'll receive the response from the system's FileChooser. If you are receiving
     * repsonses from other external Activities, you'll need to check the requestCode
     *
     * @param requestCode the same given at startActivityForResult, we are using the value 1 for this example
     * @param resultCode tells whether the operation was successful.
     * @param intent the response Intent
     */
    @Override public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK && intent != null && intent.getData() != null) {
            Uri result = intent.getData();
            Uri[] result5 = new Uri[]{ result };
            if(inputFile != null) inputFile.onReceiveValue(result);
        }
        else if(resultCode == Activity.RESULT_CANCELED){
            if(inputFile != null) inputFile.onReceiveValue(null);
        }
        inputFile = null;
    }

    /**
     * Interface between the webview content and our Android app.
     */
    private class MyJavaScriptInterface {
        MyJavaScriptInterface() {}

        /**
         * Method called when the injected JS method upload(id,name) is called
         * from the webview
         *
         * @param id the ID attribute of the clicked HTML element
         * @param name the name attribute of the clicked HTML element
         */
        @JavascriptInterface public void upload(final String id, final String name) {
            fileChooser(new ValueCallback<Uri>() {
                @Override public void onReceiveValue(Uri uri) {
                    if( uri == null || uri.equals("") ) return;
                    kitKatWebViewClient.putFile(name, uri);
                    String fileName = KitKatWebViewClient.getRealPathFromURI(KitKatWebviewActivity.this, uri);
                    //JS call to update our button with the chosen filename
                    webView.loadUrl("javascript:$('"+id+"').value = '"+fileName+"'; void(0);");
                }
            });
        }
    }
}
