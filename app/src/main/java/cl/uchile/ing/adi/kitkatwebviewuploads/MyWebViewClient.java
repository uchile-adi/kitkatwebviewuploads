package cl.uchile.ing.adi.kitkatwebviewuploads;


import android.webkit.WebViewClient;

/**
 * This is your standard WebViewClient, here you should implement your
 * custom behavior for actions in the webview.
 */
public class MyWebViewClient extends WebViewClient {
}
