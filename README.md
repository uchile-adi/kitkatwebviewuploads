# KitKat Webview Upload file workaround  #

## Problem ##
Some currently popular Android versions do not react to click/taps on <input type="file"> tags inside a WebView. 
This is documented at [https://code.google.com/p/android/issues/detail?id=62220](https://code.google.com/p/android/issues/detail?id=62220)

The proposed solutions didn't fit our needs since we have many different views which need to upload a file using a HTML form.

## Solution ##
We found we can intercept the submit form actions before they start a connection with the server. We get all the form data and send it behind the scenes sending a HTTP POST from Android (and not from the WebView). Then we receive the response from the server and we send it back to the Webview. This solution **DOES NOT** need major changes on your HTML views at the server, we just need to manipulate the DOM using the `WebView.loadUrl()` method. You can check the sample HTML view in the `files.php` script.

This is not the ideal solution and it has a lot of extra code, but it is generic enough to fit in many other projects. You need to be aware that this workaround produce an extra method call for every webview request. We recommend to check the API level and instance KitKatWebViewClient just for the problematic OS.
The example project has the minimal  code to replicate the bug on API 19 devices and show our solution.

To implement this workaround you need to follow the next steps. We assume you already have implemented the filechooser callbacks for the other Android versions. You can find that code in the sample project anyway ;)

### Implement a custom WebViewClient class to host the KitKat-specific code###
Our approach is to create a subclass of your WebViewClient implementation, your custom webview behavior will be separated from the hacks.

```
#!java
public class KitKatWebViewClient extends MyWebViewClient {
... 
}
```


* Declare and instantiate a Map<String, Uri> object. Also implement the `public void putFile(String name, Uri uri)` method to maintain the collection of selected files to upload. We'll check how to fill this Map later.


```
#!javascript

private Map<String, Uri> files = new HashMap<String, Uri>();
public void putFile(String name, Uri uri) { this.files.put(name, uri);  }
```


* At the end of the `public void onPageFinished(WebView view, String url)` method you need to execute the needed JavaScript statements to implement the workaround.
    * Convert all your <input type="file"> into <input type="button">. 
    * Define an onclick behavior to call the method `KitKatUploads.upload(id, name)`. This method is an interface to our Android code. The **id** is the HTML attribute 'id' and **name** is the HTML attribute 'name' of the clicked button.
    * Change the form's method attribute from POST to GET. You need to generate a GET request to capture the non-file form field values.
    * Add the <input type="hidden" name="shouldInterceptRequest" value="1"> tag into the form to hint Android you need to perform the workaround for this request when pressing the submit button. 

All this manipulation can be easily done with a front-end library like Prototype or JQuery. Our example uses Prototype. 

```
#!java
public void onPageFinished(WebView view, String url){
        super.onPageFinished(view,url); //Do not forget to call super to execute your own onPageFinished statements.
        String chooseFileStr = activity.getString(R.string.webview_file_chooser);
        view.loadUrl(... JavaScript transforming the DOM ...); //See the snippet below
    }

```


```
#!javascript
javascript:$$('input[type=\"file\"]').each(
    function(f){
        var f1 = f; 
        f.type='button'; 
        f.value='"+chooseFileStr+"'; 
        f.on('click', function() {KitKatUploads.upload(f1.getAttribute('id'), f1.getAttribute('name'));}); 
        f = f.up('form'); 
        f.insert('<input type=\"hidden\" name=\"shouldInterceptRequest\" value=\"1\">' ); 
        f.setAttribute('method','get'); });
```

* Implement the `public WebResourceResponse shouldInterceptRequest(WebView view, String url)` method. This method is called before any webview request. You should be only interested in requests whose URL contains the  **shouldInterceptRequest=1** field. In that case you intercept the outgoing call and create a new POST request using your favorite HTTP library. In detail you have to:
    * Check whether the **url** parameter comes with the "shouldInterceptRequest=1" query. If the parameter is not present, return null immediately to allow the webview continue the request. If the parameter exists you need to perform the HTTP POST by yourself.
    * Instantiate a POST request to the destination URL.
    * Capture the form fields at the GET query string and add them to the request. 
    * Add the files in the **files** Map as multipart data.
    * Execute the HTTP call and receive the response. If successful you need to transform the response into a WebResourceResponse and return this object. Your Webview will show the server response. 
 
We use the [OkHttp library](https://square.github.io/okhttp/) to generate the HTTP POST and process the response.

```
#!Java
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        if( ! url.contains("shouldInterceptRequest=1") ) return null;

        MultipartBuilder multipart = new MultipartBuilder().type(MultipartBuilder.FORM);

        Uri uri = Uri.parse(url);
        for( String key: uri.getQueryParameterNames() ) {
            if( ! key.equals("shouldInterceptRequest") ) multipart.addFormDataPart( key, uri.getQueryParameter(key) );
        }
        for( String name: this.files.keySet() ) {
            String filename = null;
            try {
                filename = getRealPathFromURI(activity, this.files.get(name));
                InputStream is = activity.getContentResolver().openInputStream(this.files.get(name));
                multipart.addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"" + name + "\"; filename=\"" + filename + "\""),
                        RequestBody.create(null, IOUtils.toByteArray(is))
                );
            } catch (IOException e) {
                e.printStackTrace();
                (new Handler(activity.getMainLooper())).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity.getApplicationContext(), R.string.webview_problem_loading_file, Toast.LENGTH_LONG).show();
                    }
                });
                String res = String.format(Locale.getDefault(), "<html><head></head><body><h1>Error</h1><p>We could not open file: %s</p><p>Please try another file</p></body></html>", filename);
                InputStream stream = new ByteArrayInputStream(res.getBytes());
                return new WebResourceResponse(MIME_TYPE, CHARSET, stream);
            }
        }
        Request request = new Request.Builder()
                .url(url.substring(0, url.indexOf("?")))
                .post( multipart.build() )
                .build();

        OkHttpClient client = new OkHttpClient();
        try {
            Response response = client.newCall(request).execute();
            if( response.isSuccessful() ) {
                //We try to get the mimeType and encoding from the server
                String[] ct = response.header("Content-Type", TEXT_HTML_CHARSET).split("; charset=");
                InputStream is = response.body().byteStream();
                if(ct.length==2) return new WebResourceResponse(ct[0], ct[1], is );
                else return new WebResourceResponse(MIME_TYPE, CHARSET, is );//Fallback to our default
            }
        } catch ( Exception e ) {
        }

        return null;
    }
```

### Update your Activity or Fragment which contains your Webview ###

* Declare a private instance variable for your WebViewClient and WebView

```
#!java
public class WebViewActivity extends ActionBarActivity/Activity/Fragment{
...
    private KitKatWebViewClient kitKatWebViewClient;
    private WebView webView;
...
}
```

* Implement the `public void fileChooser(ValueCallback<Uri> c)` method which invokes the system's File Chooser

```
#!java
    public void fileChooser(ValueCallback<Uri> c) {
        inputFile = c;
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("*/*");
        startActivityForResult(Intent.createChooser(i, this.getString(R.string.webview_file_chooser)), 1);
    }
```

* Create the an inner class called **MyJavaScriptInterface** and implement the `@JavascriptInterface public void upload(final String id, final String name)` method. The annotation means this method can be called from the webview's javascript engine. This method will be called when the modified <input type="files"> are tapped/clicked. In this method you need to:
    * Invoke the `fileChooser` method and define a callback implementation.
    * When the `onReceiveValue` method in invoked, that means the user has chosen a file. Use the `KitKatWebViewClient.putFile(..)` method to save the selected file into the map. You'll use this map to send the internal HTTP POST at the KitKatWebViewClient when the user presses "submit". 
    * You can also use JavaScript to change the button value and display the selected file name. With that, the user receives feedback the file was selected to upload.
```
#!java
    private class MyJavaScriptInterface {
        MyJavaScriptInterface() {}
        @JavascriptInterface public void upload(final String id, final String name) {
            fileChooser(new ValueCallback<Uri>() {
                @Override public void onReceiveValue(Uri uri) {
                    if( uri == null || uri.equals("") ) return;
                    kitKatWebViewClient.putFile(name, uri);
                    String fileName = KitKatWebViewClient.getRealPathFromURI(KitKatWebviewActivity.this, uri);
                    //JS call to update our button with the chosen filename
                    webView.loadUrl("javascript:$('"+id+"').value = '"+fileName+"'; void(0);");
                }
            });
        }
    }
```


* At the `onCreate` (Activities) or at the `onCreateView` methods get the webview instance and configure it by setting the **KitKatWebViewClient** as WebViewClient and adding our JavaScriptInterface **MyJavaScriptInterface**. 

```
#!java
    @Override protected void onCreate(Bundle savedInstanceState) {
        ...
        webView = (WebView) this.findViewById(R.id.webview);
        kitKatWebViewClient = new KitKatWebViewClient(this);
        webView.setWebViewClient(kitKatWebViewClient); //We use out new WebViewClient which includes the KitKat workaround
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "KitKatUploads");// We need to attach our Javascript interface to catch the upload() method calls
        ....
    }
```


That's all. There are many border cases not covered here. This is one way to implement this, you'll probably need to define where to put this code in order to make it compatible with your app.